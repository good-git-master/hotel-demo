package cn.itcast.hotel;

import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.text.Text;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightField;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.client.RestTemplate;

import javax.naming.directory.SearchResult;
import java.io.IOException;
import java.util.Map;

/**
 * @author 15242
 * @version 1.0
 * @project hotel-demo
 * @date 2024/3/15 11:23:03
 */
@SpringBootTest
public class QueryTest {

    public static final String HOTLE = "hotel";

    @Autowired
    private RestHighLevelClient restHighLevelClient;


    //    查询价格是 159 的酒店信息
    @Test
    void queryTest1() throws IOException {
        SearchRequest searchResult = new SearchRequest(HOTLE);
        searchResult.source().query(QueryBuilders.termQuery("price", "159"));
        SearchResponse response = restHighLevelClient.search(searchResult, RequestOptions.DEFAULT);
        SearchHits hits = response.getHits();
        SearchHit[] hits1 = hits.getHits();
        for (SearchHit documentFields : hits1) {
            String sourceAsString = documentFields.getSourceAsString();
            System.out.println("sourceAsString = " + sourceAsString);
        }
    }

    @Test
    void test2() throws IOException {
        //查询价格在100到300之间的酒店信息
        SearchRequest searchRequest = new SearchRequest(HOTLE);
        searchRequest.source().query(QueryBuilders.rangeQuery("price").gt(100).lt(500));
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            String sourceAsString = hit.getSourceAsString();
            System.out.println("sourceAsString = " + sourceAsString);
        }
    }

    @Test
    void test3() throws IOException {
        //查询名称包含如家，且价格不高于300的酒店信息
        SearchRequest searchRequest = new SearchRequest(HOTLE);
        searchRequest.source().query(QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("name", "如家"))
                .mustNot(QueryBuilders.rangeQuery("price").gt(300))
        );
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        for (SearchHit hit : response.getHits().getHits()) {
            String sourceAsString = hit.getSourceAsString();
            System.out.println("sourceAsString = " + sourceAsString);
        }
    }

    @Test
    void test4() throws IOException {
        //查询名称包含如家，且价格不高于300的酒店信息，并根据价格升序，分数降序排序
        SearchRequest searchRequest = new SearchRequest(HOTLE);
        searchRequest.source().query(QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("name", "如家"))
                .mustNot(QueryBuilders.rangeQuery("price").gt(300))
        ).sort("price", SortOrder.ASC).sort("score", SortOrder.DESC);
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        for (SearchHit hit : response.getHits().getHits()) {
            String sourceAsString = hit.getSourceAsString();
            System.out.println("sourceAsString = " + sourceAsString);
        }
    }

    @Test
    void test5() throws IOException {
//        查询酒店名称包含 如家 的第2页面且每页10条数据 酒店信息

        int page = (2 - 1) * 10;
        SearchRequest searchRequest = new SearchRequest(HOTLE);
        searchRequest.source().query(QueryBuilders.matchQuery("name", "如家"))
                .from(page)
                .size(10);
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            String sourceAsString = hit.getSourceAsString();
            System.out.println("sourceAsString = " + sourceAsString);
        }
    }

    @Test
    void test6() throws IOException {
        //查询如家高亮显示
        SearchRequest searchRequest = new SearchRequest(HOTLE);
        searchRequest.source().query(QueryBuilders.matchQuery("all", "如家"))
                .highlighter(new HighlightBuilder().field("all").preTags("<span style='color : red;'>").postTags("</span>"));
        SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
        SearchHit[] hits = response.getHits().getHits();
        for (SearchHit hit : hits) {
            Map<String, HighlightField> highlightFields = hit.getHighlightFields();
            HighlightField all = highlightFields.get("all");
            for (Text fragment : all.fragments()) {
                String string = fragment.string();
                System.out.println("string = " + string);
            }
        }
    }
}
