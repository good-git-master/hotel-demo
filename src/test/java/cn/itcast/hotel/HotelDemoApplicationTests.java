package cn.itcast.hotel;

import cn.itcast.hotel.mapper.HotelMapper;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.pojo.HotelDoc;
import com.alibaba.fastjson.JSON;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.common.xcontent.XContentType;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@SpringBootTest
class HotelDemoApplicationTests {


    @Autowired
    private HotelMapper hotelMapper;
    public static final String HOTEL = "hotel";
    public static final String SOURCE = "{\n" +
            "  \"mappings\": {\n" +
            "    \"properties\": {\n" +
            "      \"id\": {\n" +
            "        \"type\": \"keyword\"\n" +
            "      },\n" +
            "      \"name\":{\n" +
            "        \"type\": \"text\",\n" +
            "        \"analyzer\": \"ik_max_word\",\n" +
            "        \"copy_to\": \"all\"\n" +
            "      },\n" +
            "      \"address\":{\n" +
            "        \"type\": \"keyword\",\n" +
            "        \"index\": false\n" +
            "      },\n" +
            "      \"price\":{\n" +
            "        \"type\": \"integer\"\n" +
            "      },\n" +
            "      \"score\":{\n" +
            "        \"type\": \"integer\"\n" +
            "      },\n" +
            "      \"brand\":{\n" +
            "        \"type\": \"keyword\",\n" +
            "        \"copy_to\": \"all\"\n" +
            "      },\n" +
            "      \"city\":{\n" +
            "        \"type\": \"keyword\",\n" +
            "        \"copy_to\": \"all\"\n" +
            "      },\n" +
            "      \"starName\":{\n" +
            "        \"type\": \"keyword\"\n" +
            "      },\n" +
            "      \"business\":{\n" +
            "        \"type\": \"keyword\"\n" +
            "      },\n" +
            "      \"location\":{\n" +
            "        \"type\": \"geo_point\"\n" +
            "      },\n" +
            "      \"pic\":{\n" +
            "        \"type\": \"keyword\",\n" +
            "        \"index\": false\n" +
            "      },\n" +
            "      \"all\":{\n" +
            "        \"type\": \"text\",\n" +
            "        \"analyzer\": \"ik_max_word\"\n" +
            "      }\n" +
            "    }\n" +
            "  }\n" +
            "}";

    @Autowired
    private RestHighLevelClient restHighLevelClient;

    @Test
    void contextLoads() {
    }

    @Test
    void deleteIndex() throws IOException {
        DeleteIndexRequest deleteIndexRequest = new DeleteIndexRequest(HOTEL);
        restHighLevelClient.indices().delete(deleteIndexRequest, RequestOptions.DEFAULT);
    }

    @Test
    void getIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest(HOTEL);
        restHighLevelClient.indices().get(request, RequestOptions.DEFAULT);
    }

    @Test
    void exictIndex() throws IOException {
        GetIndexRequest request = new GetIndexRequest(HOTEL);
        boolean exists = restHighLevelClient.indices().exists(request, RequestOptions.DEFAULT);
        System.out.println("exists = " + exists);
    }

    @Test
    void createIndex() throws IOException {
        CreateIndexRequest request = new CreateIndexRequest(HOTEL);
        request.source(SOURCE, XContentType.JSON);
        restHighLevelClient.indices().create(request, RequestOptions.DEFAULT);
    }

    @Test
    void docById() throws IOException {
        Hotel hotel = hotelMapper.selectById(36934l);
        HotelDoc hotelDoc = new HotelDoc(hotel);
        IndexRequest request = new IndexRequest(HOTEL).id("36934");
        request.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
        restHighLevelClient.index(request, RequestOptions.DEFAULT);
    }

    @Test
    void getDoc() throws IOException {
        GetRequest request=new GetRequest(HOTEL,"36934");
        GetResponse documentFields = restHighLevelClient.get(request, RequestOptions.DEFAULT);
        System.out.println("documentFields = " + documentFields);
    }

    @Test
    void updateDoc() throws IOException {
        UpdateRequest request=new UpdateRequest();
        /*Hotel hotel = new Hotel();
        hotel.setName("九天酒店");
        request.doc(hotel,XContentType.JSON);*/

        request.doc("name","10天酒店","price","200");

        restHighLevelClient.update(request,RequestOptions.DEFAULT);
    }

    @Test
    void deleteDoc() throws IOException {
        DeleteRequest request=new DeleteRequest(HOTEL,"36934");
        restHighLevelClient.delete(request,RequestOptions.DEFAULT);
    }

    @Test
    void docBulk() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();

        hotelMapper.selectList(null)
                .stream()
                .map(HotelDoc::new)
                .collect(Collectors.toList())
                        .forEach(hotelDoc -> {
                            //构建单个请求
                            IndexRequest request = new IndexRequest(HOTEL).id(hotelDoc.getId().toString());
                            request.source(JSON.toJSONString(hotelDoc),XContentType.JSON);
                            bulkRequest.add(request);
                        });

        restHighLevelClient.bulk(bulkRequest,RequestOptions.DEFAULT);
    }

    @Test
    void testTime() throws IOException {
        BulkRequest bulkRequest = new BulkRequest();
        List<HotelDoc> collect = hotelMapper.selectList(null)
                .stream()
                .map(HotelDoc::new)
                .collect(Collectors.toList());
        int totalSize = collect.size();
        int pageSize=10;
        int  pageNumber = (int) Math.ceil((double)totalSize / pageSize); // 计算总页数


        for (int i = 1; i <= pageNumber; i++) {
            collect.stream()
                    .skip((i - 1) * pageSize) // 跳过前面的数据
                    .limit(pageSize)
                    .forEach(hotelDoc -> {
                        //构建单个请求
                        IndexRequest request = new IndexRequest(HOTEL).id(hotelDoc.getId().toString());
                        request.source(JSON.toJSONString(hotelDoc), XContentType.JSON);
                        bulkRequest.add(request);
                    });        }
        restHighLevelClient.bulk(bulkRequest,RequestOptions.DEFAULT);
    }
}








