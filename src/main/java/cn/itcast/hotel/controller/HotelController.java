package cn.itcast.hotel.controller;

import cn.itcast.hotel.common.PageResult;
import cn.itcast.hotel.dto.HotelPageQueryDTO;
import cn.itcast.hotel.service.HotelService;
import cn.itcast.hotel.vo.HotelVO;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author 15242
 * @version 1.0
 * @project hotel-demo
 * @date 2024/3/15 14:38:05
 */
@Slf4j
@RequestMapping("/hotel")
@RestController
@AllArgsConstructor
public class HotelController {

    private HotelService hotelService;

    @PostMapping("/list")
    public PageResult<HotelVO> list(@RequestBody HotelPageQueryDTO hotelPageQueryDTO){
       PageResult<HotelVO> pageResult= hotelService.listData(hotelPageQueryDTO);
       return pageResult;
    }
}
