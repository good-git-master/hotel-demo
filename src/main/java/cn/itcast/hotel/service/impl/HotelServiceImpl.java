package cn.itcast.hotel.service.impl;

import cn.itcast.hotel.common.PageResult;
import cn.itcast.hotel.dto.HotelPageQueryDTO;
import cn.itcast.hotel.mapper.HotelMapper;
import cn.itcast.hotel.pojo.Hotel;
import cn.itcast.hotel.service.HotelService;
import cn.itcast.hotel.vo.HotelVO;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.geo.GeoPoint;
import org.elasticsearch.common.lucene.search.function.CombineFunction;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.index.query.functionscore.FunctionScoreQueryBuilder;
import org.elasticsearch.index.query.functionscore.ScoreFunctionBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * 酒店服务
 *
 * @author liudo
 * @date 2023/08/15
 */
@Service
@AllArgsConstructor
@Slf4j
public class HotelServiceImpl extends ServiceImpl<HotelMapper, Hotel> implements HotelService {

    public static final String HOTEL = "hotel";
    private RestHighLevelClient restHighLevelClient;

    @Override
    public PageResult<HotelVO> listData(HotelPageQueryDTO hotelPageQueryDTO) {

        String key = hotelPageQueryDTO.getKey();
        Integer page = Objects.isNull(hotelPageQueryDTO.getPage()) ? 1 : hotelPageQueryDTO.getPage();
        Integer size = Objects.isNull(hotelPageQueryDTO.getSize()) ? 10 : hotelPageQueryDTO.getSize();
        String city = hotelPageQueryDTO.getCity();
        String starName = hotelPageQueryDTO.getStarName();
        String brand = hotelPageQueryDTO.getBrand();
        Integer maxPrice = hotelPageQueryDTO.getMaxPrice();
        Integer minPrice = hotelPageQueryDTO.getMinPrice();
        String location = hotelPageQueryDTO.getLocation();

        SearchRequest searchRequest = new SearchRequest(HOTEL);

        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        //判断搜索关键词是否非空
        if (StringUtils.isEmpty(key)) {
            boolQueryBuilder.must(QueryBuilders.matchAllQuery());
        } else {
            boolQueryBuilder.must(QueryBuilders.matchQuery("all", key));
        }
        //判断城市是否非空
        if (StringUtils.isNoneEmpty(city)) {
            boolQueryBuilder.filter(QueryBuilders.termQuery("city", city));
        }
        //判断星级是否为空
        if (StringUtils.isNoneEmpty(starName)) {
            boolQueryBuilder.filter(QueryBuilders.termQuery("starName", starName));
        }
        //判断品牌是否为空
        if (StringUtils.isNoneEmpty(brand)) {
            boolQueryBuilder.filter(QueryBuilders.termQuery("brand", brand));
        }

        if (Objects.nonNull(maxPrice) && Objects.nonNull(minPrice)) {
            boolQueryBuilder.filter(QueryBuilders.rangeQuery("price")
                    .gte(minPrice)
                    .lte(maxPrice));
        }
        //附近的人
        if(StringUtils.isNoneEmpty(location)){
            searchRequest.source()
                    .sort(SortBuilders.geoDistanceSort("location",new GeoPoint(location)).unit(DistanceUnit.KILOMETERS).order(SortOrder.ASC));
        }
        try {
            searchRequest.source()
                    .query(QueryBuilders.functionScoreQuery(boolQueryBuilder,new FunctionScoreQueryBuilder.FilterFunctionBuilder[]{
                            new FunctionScoreQueryBuilder.FilterFunctionBuilder(
                                    //参与算法的条见
                                    QueryBuilders.termQuery("brand","如家"),
                                    ScoreFunctionBuilders.weightFactorFunction(10)
                            )
                    }).boostMode(CombineFunction.SUM) )
                    .from((page - 1) * size).size(size);
            SearchResponse response = restHighLevelClient.search(searchRequest, RequestOptions.DEFAULT);
            long total = response.getHits().getTotalHits().value;
            SearchHit[] hits = response.getHits().getHits();
            List<HotelVO> hotelVOList = Arrays.stream(hits).map(item -> {
                String sourceAsString = item.getSourceAsString();
                HotelVO hotelVO = JSON.parseObject(sourceAsString, HotelVO.class);
                Object[] sortValues = item.getSortValues();
                if(sortValues.length>0){
                    hotelVO.setDistance(sortValues[0]);
                }
                hotelVO.setAD(Objects.equals("如家",hotelVO.getBrand()));
                return hotelVO;
            }).collect(Collectors.toList());
            PageResult<HotelVO> pageResult = new PageResult<>();
            pageResult.setData(hotelVOList);
            pageResult.setTotal(total);
            return pageResult;
        } catch (IOException e) {
            log.info(e.getMessage(), e);
            throw new RuntimeException(e);
        }
    }
}
